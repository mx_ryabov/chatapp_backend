import * as mongoose from 'mongoose';

export interface IBaseUser extends mongoose.Document {
    login: string,
    hashedPassword: string,
    salt: string,
    created: Date,
    secondName: string,
    firstName: string,
    lastEntered: Date,
    _plainPassword: string,
    vkId: string;
    checkPassword(password: string): boolean,
}

export interface IBlackList extends mongoose.Document {
    token: string;
}

export interface IMessage extends mongoose.Document {
    text: string;
    date: Date;
    attachmentedPhoto: string;
    from: mongoose.Schema.Types.ObjectId
}

export interface IChat extends mongoose.Document {
    users: Array<mongoose.Schema.Types.ObjectId>;
    messages: Array<mongoose.Schema.Types.ObjectId>;
}
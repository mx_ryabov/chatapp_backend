import * as mongoose from 'mongoose';
import {IChat} from '../interfaces';

const schema:mongoose.Schema = new mongoose.Schema({
    users: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "BaseUser"
    }],
    messages: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Message"
    }]
});

export interface IChatModel extends mongoose.Model<IChat> {
    getByAddressee(addressee_id:string, me_id:string, with_populate: boolean): Promise<any>;
}


schema.static("getByAddressee", async function(this: IChatModel, addressee_id:string, me_id:string, with_populate: boolean) {
    
    let populates:string[] = [];
    if (with_populate) populates = ['messages', 'users'];
    return await this.findOne({ 
        $or: [
            { users: [addressee_id, me_id] }, 
            { users: [me_id, addressee_id] }
        ]   
    }).populate(populates);
});


export const Chat:IChatModel = mongoose.model<IChat, IChatModel>('Chat', schema);

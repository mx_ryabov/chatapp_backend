interface IBaseUser {
    login: string,
    secondName: string,
    firstName: string,
    vkId: string;
    _id: string;
}

declare namespace Express {
    
    export interface Request {
        me: IBaseUser;
    }
}